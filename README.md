This repository will be used as the website for Functional Programming CS-210. It will be updated weekly throughout the semester. This README contains general information about the class.

- [previous-years-exams](https://gitlab.epfl.ch/lamp/cs210-material/week1/tree/master/previous-years-exams) contains PDFs for the 2016, 2017 and 2018 exams.
- [recitation-sessions](https://gitlab.epfl.ch/lamp/cs210-material/week1/tree/master/recitation-sessions) contains markdown documents for recitation sessions and solutions.
- [slides](https://gitlab.epfl.ch/lamp/cs210-material/week1/tree/master/slides
) contains the slides presented in class.

We will use GitLab's issue tracker as a discussion forum. Feel free to [open an issue](https://gitlab.epfl.ch/lamp/cs-210-functional-programming-2019/issues/new) if you have any comments or questions.

# IMPORTANT: First week tasks

1. [Click here to complete the Doodle to register to the recitation sessions](https://doodle.com/poll/x3iyh8czww2pcat9)
2. Follow the [Tools Setup](week1/00-tools-setup.md) page.
3. Do the [example assignment](week1/01-example.md).
4. Do the first graded assignment (not online yet, check back later)

## Grading

The grading of the course is divided between projects (30%), a midterm (30%), and a final exam (40%) held during the last week of courses.

## Staff

| Role        | People |
| :---        | :--- |
| Professors  | Martin Odersky, Viktor Kunčak |
| TAs         | Aleksander Boruch-Gruszecki, Guillaume Martres, Nicolas Stucki, Olivier Blanvillain, Periklis Chrysogelos |
| Student TAs | Artur Vignon, Haley Owsianko, Paul Renauld, Thomas De Chevigny, Timothée Floure |

## Rooms

Live lectures take place on Wednesdays from 13:15 to 15:00 in CO 2. Recitation sessions take place on Fridays from 10:15 to 12:00, where the room assignment is according to the Doodle linked above. Project sessions are held concurrently to the recitation sessions in CO 021.

## Lecture Schedule

| Week | Date        | Topic                  |
| :--  | :--         | :--                    |
| 1    | 18.09.2019  | Intro class            |
| 2    | 25.09.2019  | Recursion              |
| 3    | 02.10.2019  | Function values        |
| 4    | 09.10.2019  | Classes                |
| 5    | 16.10.2019  | Symbolic computations  |
| 6    | 23.10.2019  | Collection             |
| 7    | 30.10.2019  | Structural induction   |
| 8    | 06.11.2019  | Monads                 |
| 9    | 13.11.2019  | State                  |
| 10   | 20.11.2019  | Constraints            |
| 11   | 27.11.2019  | Lambda calculus / Lisp |
| 12   | 04.12.2019  | Scheme interpreter     |
| 13   | 11.12.2019  | Prolog                 |
| 14   | 18.12.2019  | Review                 |

## Project Schedule

| Title                | Start Date | Due Date (at noon) |
| :--                  | :--        | :--                |
| Recursion            | 18.09.2019 | 26.09.2019         |
| Functional Sets      | 25.09.2019 | 03.10.2019         |
| Object-Oriented Sets | 02.10.2019 | 10.10.2019         |
| Huffman Coding       | 09.10.2019 | 24.10.2019         |
| Anagrams             | 16.10.2019 | 31.10.2019         |
| Bloxorz              | 23.10.2019 | 07.11.2019         |
| Quickcheck           | 30.10.2019 | 14.11.2019         |
| Calculator           | 13.11.2019 | 28.11.2019         |
| Interpreter          | 27.11.2019 | 18.12.2019         |

## Recitation Sessions Schedule

| Title                | Handout Released | Live Session | Solution Release |
| :--                  | :--              | :--          | :--              |
| Recitation Session 1 | -                | 04.10.2019   | 07.10.2019       |
| Recitation Session 2 | 07.10.2019       | 11.10.2019   | 14.10.2019       |
| Recitation Session 3 | 14.10.2019       | 18.10.2019   | 22.10.2019       |
| Recitation Session 4 | 22.10.2019       | 25.10.2019   | 28.10.2019       |
| Recitation Session 5 | 28.10.2019       | 01.11.2019   | 04.11.2019       |
| Recitation Session 6 | 04.11.2019       | 15.11.2019   | 18.11.2019       |
| Recitation Session 7 | 18.11.2019       | 22.11.2019   | 25.11.2019       |
| Recitation Session 8 | 25.11.2019       | 29.11.2019   | 02.12.2019       |

## Exam Schedule

The midterm exam will take place on Friday 08.11.2019 from 10:15 to 12:00.
The final exam will take place on Friday 20.12.2019 from 10:15 to 12:00.

Information about exam organization will be communicated by email a few days before each exam.
